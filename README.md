# UPLOAD prototype

Ce dépôt contient les ébauches de design, ainsi qu'un prototype semi-statique du site de mutualisation des ressources.

Les wireframes du design sont fait avec [Pencil](http://pencil.evolus.vn/), logiciel libre de design d'interfaces graphiques.

Pour réaliser le prototype, j'ai utilisé [Mavo](http://mavo.io/), un outil libre qui permet la création d'applications web à l'aide de balises et d'attributs XML dans une page HTML.
